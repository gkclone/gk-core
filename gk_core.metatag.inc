<?php

/**
 * @file
 * Metatag integration for gk_core.
 */

/**
 * Implements hook_metatag_info().
 */
function gk_core_metatag_info() {
  $info['groups']['refresh'] = array(
    'label' => t('Refresh'),
  );

  $info['tags']['refresh'] = array(
    'label' => t('Refresh'),
    'description' => t('The number of seconds to wait before refreshing the page'),
    'class' => 'DrupalTextMetaTag',
    'group' => 'refresh',
    'element' => array(
      '#theme' => 'metatag_refresh',
    ),
  );

  return $info;
}
