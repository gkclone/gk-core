<?php

class NodeEntityQuery extends EntityFieldQuery {
  public function __construct($bundle = NULL, $exclude = TRUE, $order = array(), $limit = NULL) {
    // Set a few defaults for this query.
    $this
      ->entityCondition('entity_type', 'node')
      ->propertyCondition('status', 1)
      ->addTag('NodeEntityQuery');

    // Specify a bundle for this query if one was provided.
    if ($bundle) {
      $this->setBundle($bundle);
    }

    // Exclude the current node? Applies to node view pages.
    if ($exclude) {
      $this->excludeCurrentNode();
    }

    // Set the ORDER BY clause. Add defaults for 'sticky' and 'created'.
    if (!isset($order['property']['sticky'])) {
      $order['property']['sticky'] = array(
        'name' => 'sticky',
        'direction' => 'DESC',
      );
    }

    if (!isset($order['property']['created'])) {
      $order['property']['created'] = array(
        'name' => 'created',
        'direction' => 'DESC',
      );
    }

    // Limit the number of results?
    if (is_numeric($limit)) {
      $this->range(0, $limit);
    }

    return $this->orderBy($order);
  }

  public function execute($node_load = TRUE) {
    $return = array();

    // Execute the EntityFieldQuery.
    $result = parent::execute();

    // If this was a count query then the result is just a number, otherwise we
    // have an array of stub nodes.
    if ($this->count) {
      $return = $result;
    }
    elseif ($result) {
      $return = $result['node'];

      if ($node_load) {
        $nids = array_keys($result['node']);
        $return = node_load_multiple($nids);
      }
    }

    // Allow modules to dip in and change the return value.
    // TODO: since we're dealing in OO, this would be better placed as an array
    // of hooks stored in the class instance ($this->postExecuteHooks).
    drupal_alter('node_entity_query_return', $return, $this);

    return $return;
  }

  /**
   * Add an entity condition to the query for the bundle property.
   *
   * @param $bundle Mixed
   *   The name of a bundle or an array of bundles.
   */
  public function setBundle($bundle) {
    return $this->entityCondition('bundle', $bundle);
  }

  /**
   * Work out the current node's ID (if any) and exclude it from the results.
   */
  public function excludeCurrentNode() {
    if ($node = menu_get_object()) {
      $this->excludeNode($node->nid);
    }

    return $this;
  }

  /**
   * Exclude the given node ID from the results.
   */
  public function excludeNode($nid) {
    $this->propertyCondition('nid', $nid, '<>');
    return $this;
  }

  /**
   * Set the ORDER BY clause
   */
  public function orderBy($order) {
    foreach ($order as $type => $fields) {
      foreach ($fields as $field) {
        $direction = isset($field['direction']) ? $field['direction'] : 'ASC';

        switch ($type) {
          case 'property':
            $this->propertyOrderBy($field['name'], $direction);
          break;

          case 'field':
            $column = isset($field['column']) ? $field['column'] : 'value';
            $this->fieldOrderBy($field['name'], $column, $direction);
          break;
        }
      }
    }

    return $this;
  }
}
