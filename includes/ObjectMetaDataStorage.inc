<?php

class ObjectMetaDataStorage {
  protected static $storage = array();

  private function __construct() {
  }

  public static function set($object, array $metadata) {
    $hash = self::objectHash($object);

    self::$storage[$hash] = array(
      'object' => $object,
      'metadata' => $metadata,
    );
  }

  public static function get($object) {
    $hash = self::objectHash($object);

    if (isset(self::$storage[$hash])) {
      return self::$storage[$hash];
    }
  }

  public static function getMetaData($object) {
    if ($storage = self::get($object)) {
      return $storage['metadata'];
    }
  }

  private static function objectHash($object) {
    return spl_object_hash($object);
  }
}
