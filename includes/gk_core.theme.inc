<?php

/**
 * @file
 * Theming functions for the Core module.
 */

/**
 * Preprocess variables for theme_menu_tree().
 */
function gk_core_preprocess_menu_tree(&$variables) {
  // This runs before Drupal cores implementation of preprocess_menu_tree().
  // We need to preserve the original tree in case it's needed later
  // (template_preprocess_menu_tree() overwrites it).
  $variables['original_tree'] = $variables['tree'];
}

/**
 * Preprocess variables for theme_image_style().
 */
function gk_core_preprocess_image_style(&$variables) {
  if (isset($variables['style_name'])) {
    $variables['attributes']['class'][] = 'image--' . $variables['style_name'];

    if (in_array($variables['style_name'], array(
      'gk_one_half',
      'gk_one_third',
      'gk_two_thirds',
      'gk_one_quarter',
      'gk_three_quarters',
    ))) {
      $class = str_replace('_', '-', str_replace('gk_', 'desk--', $variables['style_name']));
      $variables['attributes']['class'][] = $class;
    }
  }
}

/**
 * Process variables for gk-core-slideshow.tpl.php
 */
function template_preprocess_gk_core_slideshow(&$variables) {
  $variables['attributes_array'] = array(
    'class' => array('gk-core-slideshow', 'cycle-slideshow', 'auto'),
    'data-cycle-fx' => $variables['settings']['effect'],
    'data-cycle-timeout' => $variables['settings']['timeout'],
    'data-cycle-slides' => '.gk-core-slideshow__slide',
    'data-cycle-pager' => '.gk-core-slideshow__pager',
    'data-cycle-pager-template' => '',
    'data-cycle-loader' => true,
    'data-cycle-progressive' => "#gk-core-slides",
    //'data-cycle-pause-on-hover' => 'true',
  );

  $variables['slides'] = array();

  foreach ($variables['items'] as $item) {
    $variables['slides'][] = theme('gk_core_slideshow_slide', array(
      'item' => $item,
      'settings' => $variables['settings'],
    ));
  }

  drupal_add_js(libraries_get_path('cycle2') . '/jquery.cycle2.min.js', array('scope' => 'footer'));
  drupal_add_js(drupal_get_path('module', 'gk_core') . '/theme/js/gk_core.slideshow.js', array('scope' => 'footer'));
}

/**
 * Process variables for gk-core-slide.tpl.php
 */
function template_preprocess_gk_core_slideshow_slide(&$variables) {
  $variables['image'] = theme('image_style', array(
    'path' => $variables['item']['uri'],
    'alt' => $variables['item']['alt'],
    'width' => $variables['item']['width'],
    'height' => $variables['item']['height'],
    'style_name' => $variables['settings']['image_style'],
    'attributes' => array(
      'class' => array('gk-core-slideshow-slide__image'),
    ),
  ));

  $variables['title'] = $variables['item']['title'];
  $variables['description'] = $variables['item']['alt'];
}

/**
 * Theme callback for the refresh meta tag.
 */
function theme_metatag_refresh($variables) {
  $output = '';
  $element = &$variables['element'];

  element_set_attributes($element, array(
    '#name' => 'http-equiv',
    '#value' => 'content',
  ));

  unset($element['#value']);
  return theme('html_tag', $variables);
}
