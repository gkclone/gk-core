<div<?php print $attributes; ?>>
  <div class="gk-core-slideshow__progress"></div>
  <div class="gk-core-slideshow__pager">
    <?php foreach ($slides as $slide): ?>
      <span></span>
    <?php endforeach; ?>
  </div>

  <?php print array_shift($slides); ?>

  <script id="gk-core-slides" type="text/cycle" data-cycle-split="---">
    <?php foreach ($slides as $slide): ?>
      <?php print $slide; ?>
      ---
    <?php endforeach; ?>
  </script>
</div>
